//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "EPLHub.h"

Define_Module(EPLHub);

void EPLHub::initialize()
{
    // Initialize parameters
    hubDelayMean = par("hubDelayMean").doubleValue();
    hubDelayStdDev = par("hubDelayStdDev").doubleValue();

    numPorts = gateSize("ethg");
    inputGateBaseId = gateBaseId("ethg$i");
    outputGateBaseId = gateBaseId("ethg$o");

    // ensure we receive frames when their first bits arrive
    for (int i = 0; i < numPorts; i++)
        gate(inputGateBaseId + i)->setDeliverOnReceptionStart(true);
}

void EPLHub::handleMessage(cMessage *msg)
{
    if (!msg->isSelfMessage())
    {
        // Handle frame sent down from the network entity: send out on every other port
        cGate *arrivalPort = msg->getArrivalGate();
        EV << "Frame " << msg << " arrived on port " << arrivalPort->getIndex() << ", broadcasting on all other ports\n";

        if (numPorts <= 1)
        {
            delete msg;
            return;
        }

        msg->setContextPointer(arrivalPort);
        //hubDelay = truncnormal(500e-9, 14e-9);
        double hubDelay = truncnormal(hubDelayMean, hubDelayStdDev);
        scheduleAt(simTime()+hubDelay, msg);
    }
    else
    {
        cGate *arrival = (cGate *)msg->getContextPointer();
        int arrivalPortIndex = arrival->getIndex();

        for (int i = 0; i < numPorts; i++)
        {
            if (i != arrivalPortIndex)
            {
                cGate *ogate = gate(outputGateBaseId + i);
                if (!ogate->isConnected())
                    continue;

                bool isLast = (arrivalPortIndex == numPorts-1) ? (i == numPorts-2) : (i == numPorts-1);
                cMessage *msg2 = isLast ? msg : msg->dup();

                // stop current transmission
                ogate->getTransmissionChannel()->forceTransmissionFinishTime(SIMTIME_ZERO);

                // send
                send(msg2, ogate);

                if (isLast)
                    msg = NULL;  // msg sent, do not delete it.
            }
        }
        delete msg;
    }
}
