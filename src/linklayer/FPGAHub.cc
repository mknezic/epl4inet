//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "FPGAHub.h"

Define_Module(FPGAHub);

void FPGAHub::initialize()
{
    // Initialize parameters
    hubDelayMean = par("hubDelayMean").doubleValue();
    hubDelayStdDev = par("hubDelayStdDev").doubleValue();

    numPorts = gateSize("port");
    inputGateBaseId = gateBaseId("port$i");
    outputGateBaseId = gateBaseId("port$o");

    // ensure we receive frames when their first bits arrive
    for (int i = 0; i < numPorts; i++)
        gate(inputGateBaseId + i)->setDeliverOnReceptionStart(true);

    gate("port_int$i")->setDeliverOnReceptionStart(true);
}

void FPGAHub::handleMessage(cMessage *msg)
{
    if (!msg->isSelfMessage())
    {
        // Handle frame sent down from the network entity: send out on every other port
        cGate *arrivalPort = msg->getArrivalGate();

        if (arrivalPort->getId() != gate("port_int$i")->getId())
        {
            // send the message to the internal port immediately
            gate("port_int$o")->getTransmissionChannel()->forceTransmissionFinishTime(SIMTIME_ZERO);
            send(msg->dup(), "port_int$o");

            if (numPorts <= 1)
            {
                delete msg;
                return;
            }
        }

        // send delayed message to the other port(s)
        msg->setContextPointer(arrivalPort);
        //hubDelay = truncnormal(2.133e-6, 14e-9);
        double hubDelay = truncnormal(hubDelayMean, hubDelayStdDev);
        scheduleAt(simTime()+hubDelay, msg);
    }
    else
    {
        cGate *arrival = (cGate *)msg->getContextPointer();
        int arrivalPortIndex = arrival->getIndex();

        if (arrival->getId() == gate("port_int$i")->getId())
        {
            // received on internal port, forward to all ports
            for (int i = 0; i < numPorts; i++)
            {
                cGate *ogate = gate(outputGateBaseId + i);
                if (!ogate->isConnected())
                    continue;

                bool isLast = (arrivalPortIndex == numPorts-1) ? (i == numPorts-2) : (i == numPorts-1);
                cMessage *msg2 = isLast ? msg : msg->dup();

                // stop current transmission
                ogate->getTransmissionChannel()->forceTransmissionFinishTime(SIMTIME_ZERO);

                // send
                send(msg2, ogate);

                if (isLast)
                    msg = NULL;  // msg sent, do not delete it.
            }
            delete msg;
        }
        else
        {
            // received on an external port, forward to other external ports
            for (int i = 0; i < numPorts; i++)
            {
                if (i != arrivalPortIndex)
                {
                    cGate *ogate = gate(outputGateBaseId + i);
                    if (!ogate->isConnected())
                        continue;

                    bool isLast = (arrivalPortIndex == numPorts-1) ? (i == numPorts-2) : (i == numPorts-1);
                    cMessage *msg2 = isLast ? msg : msg->dup();

                    // stop current transmission
                    ogate->getTransmissionChannel()->forceTransmissionFinishTime(SIMTIME_ZERO);

                    // send
                    send(msg2, ogate);

                    if (isLast)
                        msg = NULL;  // msg sent, do not delete it.
                }
            }
            delete msg;
        }
    }
}
