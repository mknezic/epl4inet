//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __EPL4INET_MNDLL_H_
#define __EPL4INET_MNDLL_H_

#include <omnetpp.h>
#include "EPLFrame_m.h"

typedef struct {
    int nodeId;
    bool chainedStation;
    bool etprcEnabled;
    double presTimeout;
    int preqActPayload;
    int multipleCycleAssign;
} NodeCollection;

class MNDll : public cSimpleModule
{
private:
    // parameters
    int nodeId;
    double cycleLenMean;
    double cycleLenStdDev;
    double waitSoc2Preq;
    double presTimeout;
    bool prcEnabled;
    double PRCSlotTimeout;
    int presMNActPayload;
    int multipleCycleCnt;
    int nodeCountStandard;
    int nodeCountMultiplexed;
    int nodeCountChained;
    int nodeIndex;
    int nodeIndexMultiplexed;
    int cycleCount;
    NodeCollection nodesStandard[255];
    NodeCollection nodesMultiplexed[255];
    NodeCollection nodesChained[255];

    // self-messages
    cMessage *socTimer;
    cMessage *presTimer;

    // additional variables
    simtime_t timestamp;
    simtime_t timestampRT;

    // receive statistics
    simtime_t isochronousPhaseDuration;
    static simsignal_t isochronousPhaseDurationSignal;
    simtime_t responseTimeMN;
    static simsignal_t responseTimeMNSignal;

public:
    MNDll();
    virtual ~MNDll();
protected:
    virtual void initialize();
    virtual void parseXMLFile(cXMLElement *root);
    virtual void handleMessage(cMessage *msg);
    virtual void handleSelfMessage(cMessage *msg);
    virtual void sendSoCFrame();
    virtual void sendNextMsg();
    virtual void sendSoAFrame();
    virtual void sendPreqFrame(int nodeId, int preqActPayload);
    virtual void handlePresFrame(PresFrame *frame);
};

#endif
