//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "MNDll.h"
#include "Ieee802Ctrl_m.h"
#include "MACAddress.h"

Define_Module(MNDll);

simsignal_t MNDll::isochronousPhaseDurationSignal = SIMSIGNAL_NULL;
simsignal_t MNDll::responseTimeMNSignal = SIMSIGNAL_NULL;

MNDll::MNDll()
{
}

MNDll::~MNDll()
{
    cancelAndDelete(socTimer);
    cancelAndDelete(presTimer);
}

void MNDll::initialize()
{
    // Initialize parameters
    nodeId = par("nodeId").longValue();
    cycleLenMean = par("cycleLenMean").doubleValue();
    cycleLenStdDev = par("cycleLenStdDev").doubleValue();
    waitSoc2Preq = par("waitSoc2Preq").doubleValue()+5.76e-6;
    if (waitSoc2Preq < 6.72e-6)
        waitSoc2Preq = 6.72e-6;
    prcEnabled = par("prcEnabled").boolValue();
    PRCSlotTimeout = par("PRCSlotTimeout").doubleValue();
    // this parameter is automatically calculated so NED parameter is obsolete now
    //presMNActPayload = par("presMNActPayload").longValue();
    presMNActPayload = 0;
    multipleCycleCnt = par("multipleCycleCnt").longValue();

    // set default node count values
    nodeCountStandard = 0;
    nodeCountChained = 0;
    nodeCountMultiplexed = 0;

    // set default value for cycle count
    cycleCount = -1;

    cXMLElement *rootNode = par("xdcMN").xmlValue();
    parseXMLFile(rootNode);

    // required to define PResMN payload for multiplexed access
    if (multipleCycleCnt != 0)
        presMNActPayload = par("presMNActPayload").longValue();

    EV << "Number of standard nodes: " << nodeCountStandard << "\n";
    EV << "Number of chained nodes: " << nodeCountChained << "\n";
    EV << "Number of multiplexed nodes: " << nodeCountMultiplexed << "\n";

    // Create self-messages
    socTimer = new cMessage("socTimer");
    presTimer = new cMessage("presTimer");

    // statistics
    isochronousPhaseDurationSignal = registerSignal("isochronousPhaseDuration");
    WATCH(isochronousPhaseDuration);
    responseTimeMNSignal = registerSignal("responseTimeMN");
    WATCH(responseTimeMN);

    // send the first SoC frame
    sendSoCFrame();
}

void MNDll::parseXMLFile(cXMLElement *root)
{
    if (root->hasChildren())
    {
        for (cXMLElement *item = root->getFirstChildWithTag("Node"); item; item = item->getNextSiblingWithTag("Node"))
        {
            NodeCollection node;

            node.nodeId = (int)(atoi(item->getAttribute("NodeId")));
            node.chainedStation = strcmp(item->getAttribute("ChainedStation"), "false") ? true : false;
            node.etprcEnabled = strcmp(item->getAttribute("EventTriggered"), "false") ? true : false;
            node.presTimeout = (double)(atof(item->getAttribute("PResTimeout")))*1e-9;
            node.preqActPayload = (int)(atoi(item->getAttribute("PReqActPayload")));

            if (multipleCycleCnt != 0)
                node.multipleCycleAssign = (int)(atoi(item->getAttribute("MultipleCycleAssign")));
            else
                node.multipleCycleAssign = 0;

            if (node.chainedStation)
                presMNActPayload += node.preqActPayload;


            if (node.chainedStation && !node.etprcEnabled)
                nodesChained[nodeCountChained++] = node;
            else if (node.multipleCycleAssign != 0)
                nodesMultiplexed[nodeCountMultiplexed++] = node;
            else
                nodesStandard[nodeCountStandard++] = node;
        }
    }
    else
        throw cRuntimeError("The module cannot be initialized. Please provide correct XML file for the MN.");
}

void MNDll::handleMessage(cMessage *msg)
{
    if (msg->isSelfMessage())
        handleSelfMessage(msg);
    else
    {
        if (dynamic_cast<PresFrame *>(msg))
            handlePresFrame(check_and_cast<PresFrame *>(msg));
        else
            // all other messages (SoA, ASnd, etc.) are ignored for now
            delete msg;
    }
}

void MNDll::handleSelfMessage(cMessage *msg)
{
    EV << "Self-message " << msg << " received\n";

    if (msg == socTimer)
        sendSoCFrame();
    else if (msg == presTimer)
        sendNextMsg();
    else
        throw cRuntimeError("Unknown self message received!");
}

void MNDll::sendSoCFrame()
{
    // create and send new SoC frame
    SocFrame *socFrame = new SocFrame("SocFrame");

    MACAddress destMACAddress;
    Ieee802Ctrl *etherctrl = new Ieee802Ctrl();
    etherctrl->setDest(destMACAddress.C_DLL_MULTICAST_SOC);
    etherctrl->setEtherType(ETHERTYPE_EPL);

    socFrame->setControlInfo(etherctrl);
    socFrame->setByteLength(EPL_SOC_HEADER_BYTES);

    send(socFrame, "lowerLayerOut");

    double cycleLen = truncnormal(cycleLenMean, cycleLenStdDev);
    scheduleAt(simTime()+cycleLen, socTimer);
    presTimer->setKind(0);
    scheduleAt(simTime()+waitSoc2Preq, presTimer);
    nodeIndex = 0;
    nodeIndexMultiplexed = 0;

    // handle multiplexed cycle count
    if (multipleCycleCnt > 0)
        cycleCount = (cycleCount + 1) % multipleCycleCnt;

    EV << "Cycle count: " << cycleCount << endl;
}

void MNDll::sendNextMsg()
{
    if (presTimer->getKind() == 0)
    {
        // WaitSoCPreq period expired send the first message (PResMN or PReq)
        if (prcEnabled)
        {
            // create and send PResMN
            PresFrame *presMNFrame = new PresFrame("PResMN");

            MACAddress destMACAddress;
            Ieee802Ctrl *etherctrl = new Ieee802Ctrl();
            etherctrl->setDest(destMACAddress.C_DLL_MULTICAST_PRES);
            etherctrl->setEtherType(ETHERTYPE_EPL);

            presMNFrame->setControlInfo(etherctrl);
            presMNFrame->setSrc(C_ADR_MN_DEF_NODE_ID);
            presMNFrame->setByteLength(presMNActPayload+EPL_PRES_HEADER_BYTES);

            send(presMNFrame, "lowerLayerOut");
            presTimer->setKind(240);
            scheduleAt(simTime()+PRCSlotTimeout, presTimer);

            // timestamp the start of polling part of the isochronous phase
            timestamp = simTime();
        }
        else
        {
            if (nodeCountStandard != 0)
            {
                // create and send the first PReq
                sendPreqFrame(nodesStandard[nodeIndex].nodeId, nodesStandard[nodeIndex].preqActPayload);

                // ET-PRC node
                presTimeout = truncnormal(nodesStandard[nodeIndex].presTimeout, 6e-9);
                // star topology (EPL1)
                //presTimeout = truncnormal(nodesStandard[nodeIndex].presTimeout, 217e-9);
                // star topology (18us)
                //presTimeout = truncnormal(nodesStandard[nodeIndex].presTimeout, 7.025e-6);
                // line topology (EPL2)
                //presTimeout = truncnormal(nodesStandard[nodeIndex].presTimeout, 248e-9);
                // line topology (18us)
                //presTimeout = truncnormal(nodesStandard[nodeIndex].presTimeout, 2.077e-6);
                presTimer->setKind(nodesStandard[nodeIndex].nodeId);
                scheduleAt(simTime()+presTimeout, presTimer);

                nodeIndex++;

                // timestamp the start of polling part of the isochronous phase
                timestamp = simTime();
            }
            else if (nodeCountMultiplexed != 0)
            {
                // handle the first multiplexed slot (PReq)
                while ((nodesMultiplexed[nodeIndexMultiplexed].multipleCycleAssign != (cycleCount + 1)) && (nodeIndexMultiplexed < nodeCountMultiplexed))
                    nodeIndexMultiplexed++;

                if (nodeIndexMultiplexed < nodeCountMultiplexed)
                {
                    if (!nodesMultiplexed[nodeIndexMultiplexed].etprcEnabled)
                        sendPreqFrame(nodesMultiplexed[nodeIndexMultiplexed].nodeId, nodesMultiplexed[nodeIndexMultiplexed].preqActPayload);

                    // ET-PRC node
                    presTimeout = truncnormal(nodesMultiplexed[nodeIndexMultiplexed].presTimeout, 6e-9);
                    // star topology (EPL1)
                    //presTimeout = truncnormal(nodesMultiplexed[nodeIndexMultiplexed].presTimeout, 217e-9);
                    // star topology (18us)
                    //presTimeout = truncnormal(nodesMultiplexed[nodeIndexMultiplexed].presTimeout, 7.025e-6);
                    // line topology (EPL2)
                    //presTimeout = truncnormal(nodesMultiplexed[nodeIndexMultiplexed].presTimeout, 248e-9);
                    // line topology (18us)
                    //presTimeout = truncnormal(nodesMultiplexed[nodeIndexMultiplexed].presTimeout, 2.077e-6);
                    presTimer->setKind(nodesMultiplexed[nodeIndexMultiplexed].nodeId);
                    scheduleAt(simTime()+presTimeout, presTimer);

                    nodeIndexMultiplexed++;

                    timestamp = simTime();
                }
                else
                    sendSoAFrame();
            }
            else
                sendSoAFrame();
        }
    }
    else if (presTimer->getKind() == 240)
    {
        // PRCSlotTimeout expired
        // check if there are any standard or ET-PRC nodes
        if (nodeCountStandard != 0)
        {
            // create and send the first PReq (if it is a standard node)
            // or remain silent in case of an ET-PRC node
            if (!nodesStandard[nodeIndex].etprcEnabled)
            {
                sendPreqFrame(nodesStandard[nodeIndex].nodeId, nodesStandard[nodeIndex].preqActPayload);

                if (nodesStandard[nodeIndex].nodeId == 1)
                {
                    // calculate the time
                    responseTimeMN = simTime() - timestampRT;
                    emit(responseTimeMNSignal, responseTimeMN);
                }
            }

            // ET-PRC node
            presTimeout = truncnormal(nodesStandard[nodeIndex].presTimeout, 6e-9);
            // star topology (EPL1)
            //presTimeout = truncnormal(nodesStandard[nodeIndex].presTimeout, 217e-9);
            // star topology (18us)
            //presTimeout = truncnormal(nodesStandard[nodeIndex].presTimeout, 7.025e-6);
            // line topology (EPL2)
            //presTimeout = truncnormal(nodesStandard[nodeIndex].presTimeout, 248e-9);
            // line topology (18us)
            //presTimeout = truncnormal(nodesStandard[nodeIndex].presTimeout, 2.077e-6);
            presTimer->setKind(nodesStandard[nodeIndex].nodeId);
            scheduleAt(simTime()+presTimeout, presTimer);

            nodeIndex++;
        }
        else if (nodeCountMultiplexed != 0)
        {
            // handle the first multiplexed slot after PResMN
            while ((nodesMultiplexed[nodeIndexMultiplexed].multipleCycleAssign != (cycleCount + 1)) && (nodeIndexMultiplexed < nodeCountMultiplexed))
                nodeIndexMultiplexed++;

            if (nodeIndexMultiplexed < nodeCountMultiplexed)
            {
                if (!nodesMultiplexed[nodeIndexMultiplexed].etprcEnabled)
                    sendPreqFrame(nodesMultiplexed[nodeIndexMultiplexed].nodeId, nodesMultiplexed[nodeIndexMultiplexed].preqActPayload);

                // ET-PRC node
                presTimeout = truncnormal(nodesMultiplexed[nodeIndexMultiplexed].presTimeout, 6e-9);
                // star topology (EPL1)
                //presTimeout = truncnormal(nodesMultiplexed[nodeIndexMultiplexed].presTimeout, 217e-9);
                // star topology (18us)
                //presTimeout = truncnormal(nodesMultiplexed[nodeIndexMultiplexed].presTimeout, 7.025e-6);
                // line topology (EPL2)
                //presTimeout = truncnormal(nodesMultiplexed[nodeIndexMultiplexed].presTimeout, 248e-9);
                // line topology (18us)
                //presTimeout = truncnormal(nodesMultiplexed[nodeIndexMultiplexed].presTimeout, 2.077e-6);
                presTimer->setKind(nodesMultiplexed[nodeIndexMultiplexed].nodeId);
                scheduleAt(simTime()+presTimeout, presTimer);

                nodeIndexMultiplexed++;
            }
            else
                sendSoAFrame();
        }
        else
            sendSoAFrame();
    }
    else
    {
        // handle the following nodes
        if (nodeIndex < nodeCountStandard)
        {
            // create and send the next PReq (if it is a standard node)
            // or remain silent in case of an ET-PRC node
            if (!nodesStandard[nodeIndex].etprcEnabled)
            {
                sendPreqFrame(nodesStandard[nodeIndex].nodeId, nodesStandard[nodeIndex].preqActPayload);

//                if (preqFrame->getDst() == 80)
//                {
//                    // calculate the time
//                    responseTimeMN = simTime() - timestampRT;
//                    emit(responseTimeMNSignal, responseTimeMN);
//                }
            }

            // ET-PRC node
            presTimeout = truncnormal(nodesStandard[nodeIndex].presTimeout, 6e-9);
            // star topology (EPL1)
            //presTimeout = truncnormal(nodesStandard[nodeIndex].presTimeout, 1610e-9);
            // star topology (18us)
            //presTimeout = truncnormal(nodesStandard[nodeIndex].presTimeout, 6.976e-6);
            // line topology (EPL2)
            //presTimeout = truncnormal(nodesStandard[nodeIndex].presTimeout, 454e-9);
            // line topology (18us)
            //presTimeout = truncnormal(nodesStandard[nodeIndex].presTimeout, 1.298e-6);
            presTimer->setKind(nodesStandard[nodeIndex].nodeId);
            scheduleAt(simTime()+presTimeout, presTimer);

            nodeIndex++;
        }
        else if (nodeIndexMultiplexed < nodeCountMultiplexed)
        {
            // handle multiplexed nodes
            while ((nodesMultiplexed[nodeIndexMultiplexed].multipleCycleAssign != (cycleCount + 1)) && (nodeIndexMultiplexed < nodeCountMultiplexed))
                nodeIndexMultiplexed++;

            if (nodeIndexMultiplexed < nodeCountMultiplexed)
            {
                if (!nodesMultiplexed[nodeIndexMultiplexed].etprcEnabled)
                    sendPreqFrame(nodesMultiplexed[nodeIndexMultiplexed].nodeId, nodesMultiplexed[nodeIndexMultiplexed].preqActPayload);

                // ET-PRC node
                presTimeout = truncnormal(nodesMultiplexed[nodeIndexMultiplexed].presTimeout, 6e-9);
                // star topology (EPL1)
                //presTimeout = truncnormal(nodesMultiplexed[nodeIndexMultiplexed].presTimeout, 217e-9);
                // star topology (18us)
                //presTimeout = truncnormal(nodesMultiplexed[nodeIndexMultiplexed].presTimeout, 7.025e-6);
                // line topology (EPL2)
                //presTimeout = truncnormal(nodesMultiplexed[nodeIndexMultiplexed].presTimeout, 248e-9);
                // line topology (18us)
                //presTimeout = truncnormal(nodesMultiplexed[nodeIndexMultiplexed].presTimeout, 2.077e-6);
                presTimer->setKind(nodesMultiplexed[nodeIndexMultiplexed].nodeId);
                scheduleAt(simTime()+presTimeout, presTimer);

                nodeIndexMultiplexed++;
            }
            else
                sendSoAFrame();
        }
        else
        {
            sendSoAFrame();
//            // calculate the time
//            responseTimeMN = simTime() - timestampRT;
//            emit(responseTimeMNSignal, responseTimeMN);
        }
    }
}

void MNDll::sendSoAFrame()
{
    // create and send new SoA frame
    SoaFrame *soaFrame = new SoaFrame("SoaFrame");

    MACAddress destMACAddress;
    Ieee802Ctrl *etherctrl = new Ieee802Ctrl();
    etherctrl->setDest(destMACAddress.C_DLL_MULTICAST_SOA);
    etherctrl->setEtherType(ETHERTYPE_EPL);

    soaFrame->setControlInfo(etherctrl);
    soaFrame->setByteLength(EPL_SOA_HEADER_BYTES);

    send(soaFrame, "lowerLayerOut");
}

void MNDll::sendPreqFrame(int nodeId, int preqActPayload)
{
    // create and send new PReq frame
    PreqFrame *preqFrame = new PreqFrame("PReq");

    MACAddress destMACAddress;
    Ieee802Ctrl *etherctrl = new Ieee802Ctrl();
    etherctrl->setDest(destMACAddress.BROADCAST_ADDRESS);
    etherctrl->setEtherType(ETHERTYPE_EPL);

    preqFrame->setControlInfo(etherctrl);
    preqFrame->setDst(nodeId);
    preqFrame->setByteLength(preqActPayload+EPL_PREQ_HEADER_BYTES);

    send(preqFrame, "lowerLayerOut");
}

void MNDll::handlePresFrame(PresFrame *frame)
{
    //if ((frame->getSrc() == nodesStandard[nodeCountStandard-1].nodeId) || (frame->getSrc() == nodesChained[nodeCountChained-1].nodeId))
    //if (frame->getSrc() == 78)
    if (frame->getSrc() == 80)
    //if ((frame->getSrc() == 10) || (frame->getSrc() == 20) || (frame->getSrc() == 30) || (frame->getSrc() == 40) || (frame->getSrc() == 50) || (frame->getSrc() == 60) || (frame->getSrc() == 70) || (frame->getSrc() == 80))
    {
        // calculate the time
        isochronousPhaseDuration = simTime() - timestamp;
        emit(isochronousPhaseDurationSignal, isochronousPhaseDuration);
    }

    /*if (frame->getSrc() == 80)
    {
        // get a timestamp of a PRes frame
        timestampRT = simTime();
    }*/

//    if (frame->getSrc() == 80)
//    {
//        // calculate the time
//        responseTimeMN = simTime() - timestampRT;
//        emit(responseTimeMNSignal, responseTimeMN);
//    }

    delete frame;
}
