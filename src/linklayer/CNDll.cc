//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "CNDll.h"
#include "Ieee802Ctrl_m.h"
#include "MACAddress.h"

Define_Module(CNDll);

simsignal_t CNDll::responseTimeCNSignal = SIMSIGNAL_NULL;

CNDll::CNDll()
{
}

CNDll::~CNDll()
{
    cancelAndDelete(responseTimer);
    cancelAndDelete(presTimer);
}

void CNDll::initialize()
{
    // Initialize parameters
    nodeId = par("nodeId").longValue();
    prevNodeId = par("prevNodeId").longValue();
    presActPayload = par("presActPayload").longValue();
    responseTimeMean = par("responseTimeMean").doubleValue();
    responseTimeStdDev = par("responseTimeStdDev").doubleValue();
    presTimeMean = par("presTime").doubleValue();
    prcEnabled = par("prcEnabled").boolValue();
    etprcEnabled = par("etprcEnabled").boolValue();

    // Create self-messages
    responseTimer = new cMessage("responseTimer");
    presTimer = new cMessage("presTimer");

    // statistics
    responseTimeCNSignal = registerSignal("responseTimeCN");
    WATCH(responseTimeCN);
}

void CNDll::handleMessage(cMessage *msg)
{
    if (msg->isSelfMessage())
        handleSelfMessage(msg);
    else
    {
        if (dynamic_cast<SocFrame *>(msg))
            handleSocFrame(check_and_cast<SocFrame *>(msg));
        else if (dynamic_cast<PreqFrame *>(msg))
        {
            if (!prcEnabled)
                handlePreqFrame(check_and_cast<PreqFrame *>(msg));
            else
                delete msg;
        }
        else if (dynamic_cast<PresFrame *>(msg))
            handlePresFrame(check_and_cast<PresFrame *>(msg));
        else
            // all other messages (SoA, ASnd, etc.) are ignored for now
            delete msg;
    }
}

void CNDll::handleSocFrame(SocFrame *frame)
{
    // process SoC frame from the MN
    delete frame;
}

void CNDll::handlePreqFrame(PreqFrame *frame)
{
    if (frame->getDst() == nodeId)
    {
        // this is for me
        double responseTime = truncnormal(responseTimeMean, responseTimeStdDev);
        scheduleAt(simTime()+responseTime, responseTimer);

        // get a timestamp of a PReq frame
        timestamp = simTime();
    }

    delete frame;
}

void CNDll::handlePresFrame(PresFrame *frame)
{
    if (etprcEnabled)
    {
        if (frame->getSrc() == prevNodeId)
        {
            // this is for me
            double responseTime = truncnormal(responseTimeMean, responseTimeStdDev);
            scheduleAt(simTime()+responseTime, responseTimer);
        }
    }
    else
    {
        if (prcEnabled && (frame->getSrc() == C_ADR_MN_DEF_NODE_ID))
        {
            // trigger PRes sending after PResTime
            double presTime = truncnormal(presTimeMean, responseTimeStdDev);
            scheduleAt(simTime()+presTime, presTimer);
        }
    }

    if (prcEnabled && (frame->getSrc() == prevNodeId))
    {
        // get a timestamp of a PRes frame
        timestamp = simTime();
    }

    delete frame;
}


void CNDll::handleSelfMessage(cMessage *msg)
{
    EV << "Self-message " << msg << " received\n";

    if ((msg == responseTimer) || (msg == presTimer))
        sendPresFrame();
    else
        throw cRuntimeError("Unknown self message received!");
}

void CNDll::sendPresFrame()
{
    // create and send new PRes frame
    PresFrame *presFrame = new PresFrame("PresFrame");

    MACAddress destMACAddress;
    Ieee802Ctrl *etherctrl = new Ieee802Ctrl();
    etherctrl->setDest(destMACAddress.C_DLL_MULTICAST_PRES);
    etherctrl->setEtherType(ETHERTYPE_EPL);

    presFrame->setControlInfo(etherctrl);
    presFrame->setSrc(nodeId);
    presFrame->setByteLength(presActPayload+EPL_PRES_HEADER_BYTES);

    send(presFrame, "lowerLayerOut");

    // calculate the response time
    responseTimeCN = simTime() - timestamp;
    //emit(responseTimeCNSignal, responseTimeCN);
}
