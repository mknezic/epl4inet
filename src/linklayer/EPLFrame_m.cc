//
// Generated file, do not edit! Created by opp_msgc 4.4 from linklayer/EPLFrame.msg.
//

// Disable warnings about unused variables, empty switch stmts, etc:
#ifdef _MSC_VER
#  pragma warning(disable:4101)
#  pragma warning(disable:4065)
#endif

#include <iostream>
#include <sstream>
#include "EPLFrame_m.h"

USING_NAMESPACE

// Template rule which fires if a struct or class doesn't have operator<<
template<typename T>
std::ostream& operator<<(std::ostream& out,const T&) {return out;}

// Another default rule (prevents compiler from choosing base class' doPacking())
template<typename T>
void doPacking(cCommBuffer *, T& t) {
    throw cRuntimeError("Parsim error: no doPacking() function for type %s or its base class (check .msg and _m.cc/h files!)",opp_typename(typeid(t)));
}

template<typename T>
void doUnpacking(cCommBuffer *, T& t) {
    throw cRuntimeError("Parsim error: no doUnpacking() function for type %s or its base class (check .msg and _m.cc/h files!)",opp_typename(typeid(t)));
}




EXECUTE_ON_STARTUP(
    cEnum *e = cEnum::find("eMsgType");
    if (!e) enums.getInstance()->add(e = new cEnum("eMsgType"));
    e->insert(MsgTypeNonPowerlink, "MsgTypeNonPowerlink");
    e->insert(MsgTypeSoc, "MsgTypeSoc");
    e->insert(MsgTypePreq, "MsgTypePreq");
    e->insert(MsgTypePres, "MsgTypePres");
    e->insert(MsgTypeSoa, "MsgTypeSoa");
    e->insert(MsgTypeAsnd, "MsgTypeAsnd");
    e->insert(MsgTypeAmni, "MsgTypeAmni");
    e->insert(MsgTypeAinv, "MsgTypeAinv");
);

Register_Class(EPLHdr);

EPLHdr::EPLHdr(const char *name, int kind) : cPacket(name,kind)
{
    this->MsgType_var = 0;
    this->Dst_var = 0;
    this->Src_var = 0;
}

EPLHdr::EPLHdr(const EPLHdr& other) : cPacket(other)
{
    copy(other);
}

EPLHdr::~EPLHdr()
{
}

EPLHdr& EPLHdr::operator=(const EPLHdr& other)
{
    if (this==&other) return *this;
    cPacket::operator=(other);
    copy(other);
    return *this;
}

void EPLHdr::copy(const EPLHdr& other)
{
    this->MsgType_var = other.MsgType_var;
    this->Dst_var = other.Dst_var;
    this->Src_var = other.Src_var;
}

void EPLHdr::parsimPack(cCommBuffer *b)
{
    cPacket::parsimPack(b);
    doPacking(b,this->MsgType_var);
    doPacking(b,this->Dst_var);
    doPacking(b,this->Src_var);
}

void EPLHdr::parsimUnpack(cCommBuffer *b)
{
    cPacket::parsimUnpack(b);
    doUnpacking(b,this->MsgType_var);
    doUnpacking(b,this->Dst_var);
    doUnpacking(b,this->Src_var);
}

int EPLHdr::getMsgType() const
{
    return MsgType_var;
}

void EPLHdr::setMsgType(int MsgType)
{
    this->MsgType_var = MsgType;
}

int EPLHdr::getDst() const
{
    return Dst_var;
}

void EPLHdr::setDst(int Dst)
{
    this->Dst_var = Dst;
}

int EPLHdr::getSrc() const
{
    return Src_var;
}

void EPLHdr::setSrc(int Src)
{
    this->Src_var = Src;
}

class EPLHdrDescriptor : public cClassDescriptor
{
  public:
    EPLHdrDescriptor();
    virtual ~EPLHdrDescriptor();

    virtual bool doesSupport(cObject *obj) const;
    virtual const char *getProperty(const char *propertyname) const;
    virtual int getFieldCount(void *object) const;
    virtual const char *getFieldName(void *object, int field) const;
    virtual int findField(void *object, const char *fieldName) const;
    virtual unsigned int getFieldTypeFlags(void *object, int field) const;
    virtual const char *getFieldTypeString(void *object, int field) const;
    virtual const char *getFieldProperty(void *object, int field, const char *propertyname) const;
    virtual int getArraySize(void *object, int field) const;

    virtual std::string getFieldAsString(void *object, int field, int i) const;
    virtual bool setFieldAsString(void *object, int field, int i, const char *value) const;

    virtual const char *getFieldStructName(void *object, int field) const;
    virtual void *getFieldStructPointer(void *object, int field, int i) const;
};

Register_ClassDescriptor(EPLHdrDescriptor);

EPLHdrDescriptor::EPLHdrDescriptor() : cClassDescriptor("EPLHdr", "cPacket")
{
}

EPLHdrDescriptor::~EPLHdrDescriptor()
{
}

bool EPLHdrDescriptor::doesSupport(cObject *obj) const
{
    return dynamic_cast<EPLHdr *>(obj)!=NULL;
}

const char *EPLHdrDescriptor::getProperty(const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : NULL;
}

int EPLHdrDescriptor::getFieldCount(void *object) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 3+basedesc->getFieldCount(object) : 3;
}

unsigned int EPLHdrDescriptor::getFieldTypeFlags(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeFlags(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
    };
    return (field>=0 && field<3) ? fieldTypeFlags[field] : 0;
}

const char *EPLHdrDescriptor::getFieldName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldNames[] = {
        "MsgType",
        "Dst",
        "Src",
    };
    return (field>=0 && field<3) ? fieldNames[field] : NULL;
}

int EPLHdrDescriptor::findField(void *object, const char *fieldName) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount(object) : 0;
    if (fieldName[0]=='M' && strcmp(fieldName, "MsgType")==0) return base+0;
    if (fieldName[0]=='D' && strcmp(fieldName, "Dst")==0) return base+1;
    if (fieldName[0]=='S' && strcmp(fieldName, "Src")==0) return base+2;
    return basedesc ? basedesc->findField(object, fieldName) : -1;
}

const char *EPLHdrDescriptor::getFieldTypeString(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeString(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldTypeStrings[] = {
        "int",
        "int",
        "int",
    };
    return (field>=0 && field<3) ? fieldTypeStrings[field] : NULL;
}

const char *EPLHdrDescriptor::getFieldProperty(void *object, int field, const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldProperty(object, field, propertyname);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        case 0:
            if (!strcmp(propertyname,"enum")) return "eMsgType";
            return NULL;
        default: return NULL;
    }
}

int EPLHdrDescriptor::getArraySize(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getArraySize(object, field);
        field -= basedesc->getFieldCount(object);
    }
    EPLHdr *pp = (EPLHdr *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

std::string EPLHdrDescriptor::getFieldAsString(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldAsString(object,field,i);
        field -= basedesc->getFieldCount(object);
    }
    EPLHdr *pp = (EPLHdr *)object; (void)pp;
    switch (field) {
        case 0: return long2string(pp->getMsgType());
        case 1: return long2string(pp->getDst());
        case 2: return long2string(pp->getSrc());
        default: return "";
    }
}

bool EPLHdrDescriptor::setFieldAsString(void *object, int field, int i, const char *value) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->setFieldAsString(object,field,i,value);
        field -= basedesc->getFieldCount(object);
    }
    EPLHdr *pp = (EPLHdr *)object; (void)pp;
    switch (field) {
        case 0: pp->setMsgType(string2long(value)); return true;
        case 1: pp->setDst(string2long(value)); return true;
        case 2: pp->setSrc(string2long(value)); return true;
        default: return false;
    }
}

const char *EPLHdrDescriptor::getFieldStructName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldStructNames[] = {
        NULL,
        NULL,
        NULL,
    };
    return (field>=0 && field<3) ? fieldStructNames[field] : NULL;
}

void *EPLHdrDescriptor::getFieldStructPointer(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructPointer(object, field, i);
        field -= basedesc->getFieldCount(object);
    }
    EPLHdr *pp = (EPLHdr *)object; (void)pp;
    switch (field) {
        default: return NULL;
    }
}

Register_Class(SocFrame);

SocFrame::SocFrame(const char *name, int kind) : EPLHdr(name,kind)
{
    this->setMsgType(MsgTypeSoc);
    this->setDst(C_ADR_BROADCAST);
    this->setSrc(C_ADR_MN_DEF_NODE_ID);

    this->MC_var = 0;
    this->PS_var = 0;
    this->NetTime_var = 0;
    this->RelativeTime_var = 0;
}

SocFrame::SocFrame(const SocFrame& other) : EPLHdr(other)
{
    copy(other);
}

SocFrame::~SocFrame()
{
}

SocFrame& SocFrame::operator=(const SocFrame& other)
{
    if (this==&other) return *this;
    EPLHdr::operator=(other);
    copy(other);
    return *this;
}

void SocFrame::copy(const SocFrame& other)
{
    this->MC_var = other.MC_var;
    this->PS_var = other.PS_var;
    this->NetTime_var = other.NetTime_var;
    this->RelativeTime_var = other.RelativeTime_var;
}

void SocFrame::parsimPack(cCommBuffer *b)
{
    EPLHdr::parsimPack(b);
    doPacking(b,this->MC_var);
    doPacking(b,this->PS_var);
    doPacking(b,this->NetTime_var);
    doPacking(b,this->RelativeTime_var);
}

void SocFrame::parsimUnpack(cCommBuffer *b)
{
    EPLHdr::parsimUnpack(b);
    doUnpacking(b,this->MC_var);
    doUnpacking(b,this->PS_var);
    doUnpacking(b,this->NetTime_var);
    doUnpacking(b,this->RelativeTime_var);
}

bool SocFrame::getMC() const
{
    return MC_var;
}

void SocFrame::setMC(bool MC)
{
    this->MC_var = MC;
}

bool SocFrame::getPS() const
{
    return PS_var;
}

void SocFrame::setPS(bool PS)
{
    this->PS_var = PS;
}

long SocFrame::getNetTime() const
{
    return NetTime_var;
}

void SocFrame::setNetTime(long NetTime)
{
    this->NetTime_var = NetTime;
}

long SocFrame::getRelativeTime() const
{
    return RelativeTime_var;
}

void SocFrame::setRelativeTime(long RelativeTime)
{
    this->RelativeTime_var = RelativeTime;
}

class SocFrameDescriptor : public cClassDescriptor
{
  public:
    SocFrameDescriptor();
    virtual ~SocFrameDescriptor();

    virtual bool doesSupport(cObject *obj) const;
    virtual const char *getProperty(const char *propertyname) const;
    virtual int getFieldCount(void *object) const;
    virtual const char *getFieldName(void *object, int field) const;
    virtual int findField(void *object, const char *fieldName) const;
    virtual unsigned int getFieldTypeFlags(void *object, int field) const;
    virtual const char *getFieldTypeString(void *object, int field) const;
    virtual const char *getFieldProperty(void *object, int field, const char *propertyname) const;
    virtual int getArraySize(void *object, int field) const;

    virtual std::string getFieldAsString(void *object, int field, int i) const;
    virtual bool setFieldAsString(void *object, int field, int i, const char *value) const;

    virtual const char *getFieldStructName(void *object, int field) const;
    virtual void *getFieldStructPointer(void *object, int field, int i) const;
};

Register_ClassDescriptor(SocFrameDescriptor);

SocFrameDescriptor::SocFrameDescriptor() : cClassDescriptor("SocFrame", "EPLHdr")
{
}

SocFrameDescriptor::~SocFrameDescriptor()
{
}

bool SocFrameDescriptor::doesSupport(cObject *obj) const
{
    return dynamic_cast<SocFrame *>(obj)!=NULL;
}

const char *SocFrameDescriptor::getProperty(const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : NULL;
}

int SocFrameDescriptor::getFieldCount(void *object) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 4+basedesc->getFieldCount(object) : 4;
}

unsigned int SocFrameDescriptor::getFieldTypeFlags(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeFlags(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
    };
    return (field>=0 && field<4) ? fieldTypeFlags[field] : 0;
}

const char *SocFrameDescriptor::getFieldName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldNames[] = {
        "MC",
        "PS",
        "NetTime",
        "RelativeTime",
    };
    return (field>=0 && field<4) ? fieldNames[field] : NULL;
}

int SocFrameDescriptor::findField(void *object, const char *fieldName) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount(object) : 0;
    if (fieldName[0]=='M' && strcmp(fieldName, "MC")==0) return base+0;
    if (fieldName[0]=='P' && strcmp(fieldName, "PS")==0) return base+1;
    if (fieldName[0]=='N' && strcmp(fieldName, "NetTime")==0) return base+2;
    if (fieldName[0]=='R' && strcmp(fieldName, "RelativeTime")==0) return base+3;
    return basedesc ? basedesc->findField(object, fieldName) : -1;
}

const char *SocFrameDescriptor::getFieldTypeString(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeString(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldTypeStrings[] = {
        "bool",
        "bool",
        "long",
        "long",
    };
    return (field>=0 && field<4) ? fieldTypeStrings[field] : NULL;
}

const char *SocFrameDescriptor::getFieldProperty(void *object, int field, const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldProperty(object, field, propertyname);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    }
}

int SocFrameDescriptor::getArraySize(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getArraySize(object, field);
        field -= basedesc->getFieldCount(object);
    }
    SocFrame *pp = (SocFrame *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

std::string SocFrameDescriptor::getFieldAsString(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldAsString(object,field,i);
        field -= basedesc->getFieldCount(object);
    }
    SocFrame *pp = (SocFrame *)object; (void)pp;
    switch (field) {
        case 0: return bool2string(pp->getMC());
        case 1: return bool2string(pp->getPS());
        case 2: return long2string(pp->getNetTime());
        case 3: return long2string(pp->getRelativeTime());
        default: return "";
    }
}

bool SocFrameDescriptor::setFieldAsString(void *object, int field, int i, const char *value) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->setFieldAsString(object,field,i,value);
        field -= basedesc->getFieldCount(object);
    }
    SocFrame *pp = (SocFrame *)object; (void)pp;
    switch (field) {
        case 0: pp->setMC(string2bool(value)); return true;
        case 1: pp->setPS(string2bool(value)); return true;
        case 2: pp->setNetTime(string2long(value)); return true;
        case 3: pp->setRelativeTime(string2long(value)); return true;
        default: return false;
    }
}

const char *SocFrameDescriptor::getFieldStructName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldStructNames[] = {
        NULL,
        NULL,
        NULL,
        NULL,
    };
    return (field>=0 && field<4) ? fieldStructNames[field] : NULL;
}

void *SocFrameDescriptor::getFieldStructPointer(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructPointer(object, field, i);
        field -= basedesc->getFieldCount(object);
    }
    SocFrame *pp = (SocFrame *)object; (void)pp;
    switch (field) {
        default: return NULL;
    }
}

Register_Class(SoaFrame);

SoaFrame::SoaFrame(const char *name, int kind) : EPLHdr(name,kind)
{
    this->setMsgType(MsgTypeSoa);
    this->setDst(C_ADR_BROADCAST);
    this->setSrc(C_ADR_MN_DEF_NODE_ID);

    this->NMTStatus_var = 0;
    this->RequestedServiceID_var = 0;
    this->RequestedServiceTarget_var = 0;
    this->EPLVersion_var = 0;
}

SoaFrame::SoaFrame(const SoaFrame& other) : EPLHdr(other)
{
    copy(other);
}

SoaFrame::~SoaFrame()
{
}

SoaFrame& SoaFrame::operator=(const SoaFrame& other)
{
    if (this==&other) return *this;
    EPLHdr::operator=(other);
    copy(other);
    return *this;
}

void SoaFrame::copy(const SoaFrame& other)
{
    this->NMTStatus_var = other.NMTStatus_var;
    this->RequestedServiceID_var = other.RequestedServiceID_var;
    this->RequestedServiceTarget_var = other.RequestedServiceTarget_var;
    this->EPLVersion_var = other.EPLVersion_var;
}

void SoaFrame::parsimPack(cCommBuffer *b)
{
    EPLHdr::parsimPack(b);
    doPacking(b,this->NMTStatus_var);
    doPacking(b,this->RequestedServiceID_var);
    doPacking(b,this->RequestedServiceTarget_var);
    doPacking(b,this->EPLVersion_var);
}

void SoaFrame::parsimUnpack(cCommBuffer *b)
{
    EPLHdr::parsimUnpack(b);
    doUnpacking(b,this->NMTStatus_var);
    doUnpacking(b,this->RequestedServiceID_var);
    doUnpacking(b,this->RequestedServiceTarget_var);
    doUnpacking(b,this->EPLVersion_var);
}

int SoaFrame::getNMTStatus() const
{
    return NMTStatus_var;
}

void SoaFrame::setNMTStatus(int NMTStatus)
{
    this->NMTStatus_var = NMTStatus;
}

int SoaFrame::getRequestedServiceID() const
{
    return RequestedServiceID_var;
}

void SoaFrame::setRequestedServiceID(int RequestedServiceID)
{
    this->RequestedServiceID_var = RequestedServiceID;
}

int SoaFrame::getRequestedServiceTarget() const
{
    return RequestedServiceTarget_var;
}

void SoaFrame::setRequestedServiceTarget(int RequestedServiceTarget)
{
    this->RequestedServiceTarget_var = RequestedServiceTarget;
}

int SoaFrame::getEPLVersion() const
{
    return EPLVersion_var;
}

void SoaFrame::setEPLVersion(int EPLVersion)
{
    this->EPLVersion_var = EPLVersion;
}

class SoaFrameDescriptor : public cClassDescriptor
{
  public:
    SoaFrameDescriptor();
    virtual ~SoaFrameDescriptor();

    virtual bool doesSupport(cObject *obj) const;
    virtual const char *getProperty(const char *propertyname) const;
    virtual int getFieldCount(void *object) const;
    virtual const char *getFieldName(void *object, int field) const;
    virtual int findField(void *object, const char *fieldName) const;
    virtual unsigned int getFieldTypeFlags(void *object, int field) const;
    virtual const char *getFieldTypeString(void *object, int field) const;
    virtual const char *getFieldProperty(void *object, int field, const char *propertyname) const;
    virtual int getArraySize(void *object, int field) const;

    virtual std::string getFieldAsString(void *object, int field, int i) const;
    virtual bool setFieldAsString(void *object, int field, int i, const char *value) const;

    virtual const char *getFieldStructName(void *object, int field) const;
    virtual void *getFieldStructPointer(void *object, int field, int i) const;
};

Register_ClassDescriptor(SoaFrameDescriptor);

SoaFrameDescriptor::SoaFrameDescriptor() : cClassDescriptor("SoaFrame", "EPLHdr")
{
}

SoaFrameDescriptor::~SoaFrameDescriptor()
{
}

bool SoaFrameDescriptor::doesSupport(cObject *obj) const
{
    return dynamic_cast<SoaFrame *>(obj)!=NULL;
}

const char *SoaFrameDescriptor::getProperty(const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : NULL;
}

int SoaFrameDescriptor::getFieldCount(void *object) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 4+basedesc->getFieldCount(object) : 4;
}

unsigned int SoaFrameDescriptor::getFieldTypeFlags(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeFlags(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
    };
    return (field>=0 && field<4) ? fieldTypeFlags[field] : 0;
}

const char *SoaFrameDescriptor::getFieldName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldNames[] = {
        "NMTStatus",
        "RequestedServiceID",
        "RequestedServiceTarget",
        "EPLVersion",
    };
    return (field>=0 && field<4) ? fieldNames[field] : NULL;
}

int SoaFrameDescriptor::findField(void *object, const char *fieldName) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount(object) : 0;
    if (fieldName[0]=='N' && strcmp(fieldName, "NMTStatus")==0) return base+0;
    if (fieldName[0]=='R' && strcmp(fieldName, "RequestedServiceID")==0) return base+1;
    if (fieldName[0]=='R' && strcmp(fieldName, "RequestedServiceTarget")==0) return base+2;
    if (fieldName[0]=='E' && strcmp(fieldName, "EPLVersion")==0) return base+3;
    return basedesc ? basedesc->findField(object, fieldName) : -1;
}

const char *SoaFrameDescriptor::getFieldTypeString(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeString(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldTypeStrings[] = {
        "int",
        "int",
        "int",
        "int",
    };
    return (field>=0 && field<4) ? fieldTypeStrings[field] : NULL;
}

const char *SoaFrameDescriptor::getFieldProperty(void *object, int field, const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldProperty(object, field, propertyname);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    }
}

int SoaFrameDescriptor::getArraySize(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getArraySize(object, field);
        field -= basedesc->getFieldCount(object);
    }
    SoaFrame *pp = (SoaFrame *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

std::string SoaFrameDescriptor::getFieldAsString(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldAsString(object,field,i);
        field -= basedesc->getFieldCount(object);
    }
    SoaFrame *pp = (SoaFrame *)object; (void)pp;
    switch (field) {
        case 0: return long2string(pp->getNMTStatus());
        case 1: return long2string(pp->getRequestedServiceID());
        case 2: return long2string(pp->getRequestedServiceTarget());
        case 3: return long2string(pp->getEPLVersion());
        default: return "";
    }
}

bool SoaFrameDescriptor::setFieldAsString(void *object, int field, int i, const char *value) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->setFieldAsString(object,field,i,value);
        field -= basedesc->getFieldCount(object);
    }
    SoaFrame *pp = (SoaFrame *)object; (void)pp;
    switch (field) {
        case 0: pp->setNMTStatus(string2long(value)); return true;
        case 1: pp->setRequestedServiceID(string2long(value)); return true;
        case 2: pp->setRequestedServiceTarget(string2long(value)); return true;
        case 3: pp->setEPLVersion(string2long(value)); return true;
        default: return false;
    }
}

const char *SoaFrameDescriptor::getFieldStructName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldStructNames[] = {
        NULL,
        NULL,
        NULL,
        NULL,
    };
    return (field>=0 && field<4) ? fieldStructNames[field] : NULL;
}

void *SoaFrameDescriptor::getFieldStructPointer(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructPointer(object, field, i);
        field -= basedesc->getFieldCount(object);
    }
    SoaFrame *pp = (SoaFrame *)object; (void)pp;
    switch (field) {
        default: return NULL;
    }
}

Register_Class(PreqFrame);

PreqFrame::PreqFrame(const char *name, int kind) : EPLHdr(name,kind)
{
    this->setMsgType(MsgTypePreq);
    this->setSrc(C_ADR_MN_DEF_NODE_ID);

    this->MS_var = 0;
    this->EA_var = 0;
    this->RD_var = 0;
    this->PDOVersion_var = 0;
    this->Size_var = 0;
}

PreqFrame::PreqFrame(const PreqFrame& other) : EPLHdr(other)
{
    copy(other);
}

PreqFrame::~PreqFrame()
{
}

PreqFrame& PreqFrame::operator=(const PreqFrame& other)
{
    if (this==&other) return *this;
    EPLHdr::operator=(other);
    copy(other);
    return *this;
}

void PreqFrame::copy(const PreqFrame& other)
{
    this->MS_var = other.MS_var;
    this->EA_var = other.EA_var;
    this->RD_var = other.RD_var;
    this->PDOVersion_var = other.PDOVersion_var;
    this->Size_var = other.Size_var;
}

void PreqFrame::parsimPack(cCommBuffer *b)
{
    EPLHdr::parsimPack(b);
    doPacking(b,this->MS_var);
    doPacking(b,this->EA_var);
    doPacking(b,this->RD_var);
    doPacking(b,this->PDOVersion_var);
    doPacking(b,this->Size_var);
}

void PreqFrame::parsimUnpack(cCommBuffer *b)
{
    EPLHdr::parsimUnpack(b);
    doUnpacking(b,this->MS_var);
    doUnpacking(b,this->EA_var);
    doUnpacking(b,this->RD_var);
    doUnpacking(b,this->PDOVersion_var);
    doUnpacking(b,this->Size_var);
}

bool PreqFrame::getMS() const
{
    return MS_var;
}

void PreqFrame::setMS(bool MS)
{
    this->MS_var = MS;
}

bool PreqFrame::getEA() const
{
    return EA_var;
}

void PreqFrame::setEA(bool EA)
{
    this->EA_var = EA;
}

bool PreqFrame::getRD() const
{
    return RD_var;
}

void PreqFrame::setRD(bool RD)
{
    this->RD_var = RD;
}

int PreqFrame::getPDOVersion() const
{
    return PDOVersion_var;
}

void PreqFrame::setPDOVersion(int PDOVersion)
{
    this->PDOVersion_var = PDOVersion;
}

int PreqFrame::getSize() const
{
    return Size_var;
}

void PreqFrame::setSize(int Size)
{
    this->Size_var = Size;
}

class PreqFrameDescriptor : public cClassDescriptor
{
  public:
    PreqFrameDescriptor();
    virtual ~PreqFrameDescriptor();

    virtual bool doesSupport(cObject *obj) const;
    virtual const char *getProperty(const char *propertyname) const;
    virtual int getFieldCount(void *object) const;
    virtual const char *getFieldName(void *object, int field) const;
    virtual int findField(void *object, const char *fieldName) const;
    virtual unsigned int getFieldTypeFlags(void *object, int field) const;
    virtual const char *getFieldTypeString(void *object, int field) const;
    virtual const char *getFieldProperty(void *object, int field, const char *propertyname) const;
    virtual int getArraySize(void *object, int field) const;

    virtual std::string getFieldAsString(void *object, int field, int i) const;
    virtual bool setFieldAsString(void *object, int field, int i, const char *value) const;

    virtual const char *getFieldStructName(void *object, int field) const;
    virtual void *getFieldStructPointer(void *object, int field, int i) const;
};

Register_ClassDescriptor(PreqFrameDescriptor);

PreqFrameDescriptor::PreqFrameDescriptor() : cClassDescriptor("PreqFrame", "EPLHdr")
{
}

PreqFrameDescriptor::~PreqFrameDescriptor()
{
}

bool PreqFrameDescriptor::doesSupport(cObject *obj) const
{
    return dynamic_cast<PreqFrame *>(obj)!=NULL;
}

const char *PreqFrameDescriptor::getProperty(const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : NULL;
}

int PreqFrameDescriptor::getFieldCount(void *object) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 5+basedesc->getFieldCount(object) : 5;
}

unsigned int PreqFrameDescriptor::getFieldTypeFlags(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeFlags(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
    };
    return (field>=0 && field<5) ? fieldTypeFlags[field] : 0;
}

const char *PreqFrameDescriptor::getFieldName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldNames[] = {
        "MS",
        "EA",
        "RD",
        "PDOVersion",
        "Size",
    };
    return (field>=0 && field<5) ? fieldNames[field] : NULL;
}

int PreqFrameDescriptor::findField(void *object, const char *fieldName) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount(object) : 0;
    if (fieldName[0]=='M' && strcmp(fieldName, "MS")==0) return base+0;
    if (fieldName[0]=='E' && strcmp(fieldName, "EA")==0) return base+1;
    if (fieldName[0]=='R' && strcmp(fieldName, "RD")==0) return base+2;
    if (fieldName[0]=='P' && strcmp(fieldName, "PDOVersion")==0) return base+3;
    if (fieldName[0]=='S' && strcmp(fieldName, "Size")==0) return base+4;
    return basedesc ? basedesc->findField(object, fieldName) : -1;
}

const char *PreqFrameDescriptor::getFieldTypeString(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeString(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldTypeStrings[] = {
        "bool",
        "bool",
        "bool",
        "int",
        "int",
    };
    return (field>=0 && field<5) ? fieldTypeStrings[field] : NULL;
}

const char *PreqFrameDescriptor::getFieldProperty(void *object, int field, const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldProperty(object, field, propertyname);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    }
}

int PreqFrameDescriptor::getArraySize(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getArraySize(object, field);
        field -= basedesc->getFieldCount(object);
    }
    PreqFrame *pp = (PreqFrame *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

std::string PreqFrameDescriptor::getFieldAsString(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldAsString(object,field,i);
        field -= basedesc->getFieldCount(object);
    }
    PreqFrame *pp = (PreqFrame *)object; (void)pp;
    switch (field) {
        case 0: return bool2string(pp->getMS());
        case 1: return bool2string(pp->getEA());
        case 2: return bool2string(pp->getRD());
        case 3: return long2string(pp->getPDOVersion());
        case 4: return long2string(pp->getSize());
        default: return "";
    }
}

bool PreqFrameDescriptor::setFieldAsString(void *object, int field, int i, const char *value) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->setFieldAsString(object,field,i,value);
        field -= basedesc->getFieldCount(object);
    }
    PreqFrame *pp = (PreqFrame *)object; (void)pp;
    switch (field) {
        case 0: pp->setMS(string2bool(value)); return true;
        case 1: pp->setEA(string2bool(value)); return true;
        case 2: pp->setRD(string2bool(value)); return true;
        case 3: pp->setPDOVersion(string2long(value)); return true;
        case 4: pp->setSize(string2long(value)); return true;
        default: return false;
    }
}

const char *PreqFrameDescriptor::getFieldStructName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldStructNames[] = {
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
    };
    return (field>=0 && field<5) ? fieldStructNames[field] : NULL;
}

void *PreqFrameDescriptor::getFieldStructPointer(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructPointer(object, field, i);
        field -= basedesc->getFieldCount(object);
    }
    PreqFrame *pp = (PreqFrame *)object; (void)pp;
    switch (field) {
        default: return NULL;
    }
}

Register_Class(PresFrame);

PresFrame::PresFrame(const char *name, int kind) : EPLHdr(name,kind)
{
    this->setMsgType(MsgTypePres);
    this->setDst(C_ADR_BROADCAST);

    this->NMTStatus_var = 0;
    this->MS_var = 0;
    this->EN_var = 0;
    this->RD_var = 0;
    this->PR_var = 0;
    this->RS_var = 0;
    this->PDOVersion_var = 0;
    this->Size_var = 0;
}

PresFrame::PresFrame(const PresFrame& other) : EPLHdr(other)
{
    copy(other);
}

PresFrame::~PresFrame()
{
}

PresFrame& PresFrame::operator=(const PresFrame& other)
{
    if (this==&other) return *this;
    EPLHdr::operator=(other);
    copy(other);
    return *this;
}

void PresFrame::copy(const PresFrame& other)
{
    this->NMTStatus_var = other.NMTStatus_var;
    this->MS_var = other.MS_var;
    this->EN_var = other.EN_var;
    this->RD_var = other.RD_var;
    this->PR_var = other.PR_var;
    this->RS_var = other.RS_var;
    this->PDOVersion_var = other.PDOVersion_var;
    this->Size_var = other.Size_var;
}

void PresFrame::parsimPack(cCommBuffer *b)
{
    EPLHdr::parsimPack(b);
    doPacking(b,this->NMTStatus_var);
    doPacking(b,this->MS_var);
    doPacking(b,this->EN_var);
    doPacking(b,this->RD_var);
    doPacking(b,this->PR_var);
    doPacking(b,this->RS_var);
    doPacking(b,this->PDOVersion_var);
    doPacking(b,this->Size_var);
}

void PresFrame::parsimUnpack(cCommBuffer *b)
{
    EPLHdr::parsimUnpack(b);
    doUnpacking(b,this->NMTStatus_var);
    doUnpacking(b,this->MS_var);
    doUnpacking(b,this->EN_var);
    doUnpacking(b,this->RD_var);
    doUnpacking(b,this->PR_var);
    doUnpacking(b,this->RS_var);
    doUnpacking(b,this->PDOVersion_var);
    doUnpacking(b,this->Size_var);
}

int PresFrame::getNMTStatus() const
{
    return NMTStatus_var;
}

void PresFrame::setNMTStatus(int NMTStatus)
{
    this->NMTStatus_var = NMTStatus;
}

bool PresFrame::getMS() const
{
    return MS_var;
}

void PresFrame::setMS(bool MS)
{
    this->MS_var = MS;
}

bool PresFrame::getEN() const
{
    return EN_var;
}

void PresFrame::setEN(bool EN)
{
    this->EN_var = EN;
}

bool PresFrame::getRD() const
{
    return RD_var;
}

void PresFrame::setRD(bool RD)
{
    this->RD_var = RD;
}

int PresFrame::getPR() const
{
    return PR_var;
}

void PresFrame::setPR(int PR)
{
    this->PR_var = PR;
}

int PresFrame::getRS() const
{
    return RS_var;
}

void PresFrame::setRS(int RS)
{
    this->RS_var = RS;
}

int PresFrame::getPDOVersion() const
{
    return PDOVersion_var;
}

void PresFrame::setPDOVersion(int PDOVersion)
{
    this->PDOVersion_var = PDOVersion;
}

int PresFrame::getSize() const
{
    return Size_var;
}

void PresFrame::setSize(int Size)
{
    this->Size_var = Size;
}

class PresFrameDescriptor : public cClassDescriptor
{
  public:
    PresFrameDescriptor();
    virtual ~PresFrameDescriptor();

    virtual bool doesSupport(cObject *obj) const;
    virtual const char *getProperty(const char *propertyname) const;
    virtual int getFieldCount(void *object) const;
    virtual const char *getFieldName(void *object, int field) const;
    virtual int findField(void *object, const char *fieldName) const;
    virtual unsigned int getFieldTypeFlags(void *object, int field) const;
    virtual const char *getFieldTypeString(void *object, int field) const;
    virtual const char *getFieldProperty(void *object, int field, const char *propertyname) const;
    virtual int getArraySize(void *object, int field) const;

    virtual std::string getFieldAsString(void *object, int field, int i) const;
    virtual bool setFieldAsString(void *object, int field, int i, const char *value) const;

    virtual const char *getFieldStructName(void *object, int field) const;
    virtual void *getFieldStructPointer(void *object, int field, int i) const;
};

Register_ClassDescriptor(PresFrameDescriptor);

PresFrameDescriptor::PresFrameDescriptor() : cClassDescriptor("PresFrame", "EPLHdr")
{
}

PresFrameDescriptor::~PresFrameDescriptor()
{
}

bool PresFrameDescriptor::doesSupport(cObject *obj) const
{
    return dynamic_cast<PresFrame *>(obj)!=NULL;
}

const char *PresFrameDescriptor::getProperty(const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : NULL;
}

int PresFrameDescriptor::getFieldCount(void *object) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 8+basedesc->getFieldCount(object) : 8;
}

unsigned int PresFrameDescriptor::getFieldTypeFlags(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeFlags(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
        FD_ISEDITABLE,
    };
    return (field>=0 && field<8) ? fieldTypeFlags[field] : 0;
}

const char *PresFrameDescriptor::getFieldName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldNames[] = {
        "NMTStatus",
        "MS",
        "EN",
        "RD",
        "PR",
        "RS",
        "PDOVersion",
        "Size",
    };
    return (field>=0 && field<8) ? fieldNames[field] : NULL;
}

int PresFrameDescriptor::findField(void *object, const char *fieldName) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount(object) : 0;
    if (fieldName[0]=='N' && strcmp(fieldName, "NMTStatus")==0) return base+0;
    if (fieldName[0]=='M' && strcmp(fieldName, "MS")==0) return base+1;
    if (fieldName[0]=='E' && strcmp(fieldName, "EN")==0) return base+2;
    if (fieldName[0]=='R' && strcmp(fieldName, "RD")==0) return base+3;
    if (fieldName[0]=='P' && strcmp(fieldName, "PR")==0) return base+4;
    if (fieldName[0]=='R' && strcmp(fieldName, "RS")==0) return base+5;
    if (fieldName[0]=='P' && strcmp(fieldName, "PDOVersion")==0) return base+6;
    if (fieldName[0]=='S' && strcmp(fieldName, "Size")==0) return base+7;
    return basedesc ? basedesc->findField(object, fieldName) : -1;
}

const char *PresFrameDescriptor::getFieldTypeString(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldTypeString(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldTypeStrings[] = {
        "int",
        "bool",
        "bool",
        "bool",
        "int",
        "int",
        "int",
        "int",
    };
    return (field>=0 && field<8) ? fieldTypeStrings[field] : NULL;
}

const char *PresFrameDescriptor::getFieldProperty(void *object, int field, const char *propertyname) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldProperty(object, field, propertyname);
        field -= basedesc->getFieldCount(object);
    }
    switch (field) {
        default: return NULL;
    }
}

int PresFrameDescriptor::getArraySize(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getArraySize(object, field);
        field -= basedesc->getFieldCount(object);
    }
    PresFrame *pp = (PresFrame *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

std::string PresFrameDescriptor::getFieldAsString(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldAsString(object,field,i);
        field -= basedesc->getFieldCount(object);
    }
    PresFrame *pp = (PresFrame *)object; (void)pp;
    switch (field) {
        case 0: return long2string(pp->getNMTStatus());
        case 1: return bool2string(pp->getMS());
        case 2: return bool2string(pp->getEN());
        case 3: return bool2string(pp->getRD());
        case 4: return long2string(pp->getPR());
        case 5: return long2string(pp->getRS());
        case 6: return long2string(pp->getPDOVersion());
        case 7: return long2string(pp->getSize());
        default: return "";
    }
}

bool PresFrameDescriptor::setFieldAsString(void *object, int field, int i, const char *value) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->setFieldAsString(object,field,i,value);
        field -= basedesc->getFieldCount(object);
    }
    PresFrame *pp = (PresFrame *)object; (void)pp;
    switch (field) {
        case 0: pp->setNMTStatus(string2long(value)); return true;
        case 1: pp->setMS(string2bool(value)); return true;
        case 2: pp->setEN(string2bool(value)); return true;
        case 3: pp->setRD(string2bool(value)); return true;
        case 4: pp->setPR(string2long(value)); return true;
        case 5: pp->setRS(string2long(value)); return true;
        case 6: pp->setPDOVersion(string2long(value)); return true;
        case 7: pp->setSize(string2long(value)); return true;
        default: return false;
    }
}

const char *PresFrameDescriptor::getFieldStructName(void *object, int field) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructName(object, field);
        field -= basedesc->getFieldCount(object);
    }
    static const char *fieldStructNames[] = {
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
    };
    return (field>=0 && field<8) ? fieldStructNames[field] : NULL;
}

void *PresFrameDescriptor::getFieldStructPointer(void *object, int field, int i) const
{
    cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount(object))
            return basedesc->getFieldStructPointer(object, field, i);
        field -= basedesc->getFieldCount(object);
    }
    PresFrame *pp = (PresFrame *)object; (void)pp;
    switch (field) {
        default: return NULL;
    }
}


