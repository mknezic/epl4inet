//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __EPL4INET_CNDLL_H_
#define __EPL4INET_CNDLL_H_

#include <omnetpp.h>
#include "EPLFrame_m.h"

class CNDll : public cSimpleModule
{
private:
    // parameters
    int nodeId;
    int prevNodeId;
    int presActPayload;
    double responseTimeMean;
    double responseTimeStdDev;
    double presTimeMean;
    bool prcEnabled;
    bool etprcEnabled;

    // self-messages
    cMessage *responseTimer;
    cMessage *presTimer;

    // additional variables
    simtime_t timestamp;

    // receive statistics
    simtime_t responseTimeCN;
    static simsignal_t responseTimeCNSignal;

public:
    CNDll();
    virtual ~CNDll();
protected:
    virtual void initialize();
    virtual void handleMessage(cMessage *msg);
    virtual void handleSocFrame(SocFrame *frame);
    virtual void handlePreqFrame(PreqFrame *frame);
    virtual void handlePresFrame(PresFrame *frame);
    virtual void handleSelfMessage(cMessage *msg);
    virtual void sendPresFrame();
};

#endif
